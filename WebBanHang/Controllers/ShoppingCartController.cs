﻿using WebBanHang.DataAccess;
using WebBanHang.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;


public class ShoppingCartController : Controller
{

    private readonly ApplicationDbContext _context;
    private readonly UserManager<ApplicationUser> _userManager;

    public ShoppingCartController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
    {
        _context = context;
        _userManager = userManager;
    }

    public IActionResult Checkout()
    {
        return View(new Order());
    }

    [HttpPost]
    public async Task<IActionResult> Checkout(Order order)
    {
        var cart = HttpContext.Session.GetObjectFromJson<ShoppingCart>("Cart");

        if (cart == null || !cart.Items.Any())
        {
            // Xử lý giỏ hàng trống...
            return RedirectToAction("Index");
        }

        var user = await _userManager.GetUserAsync(User);

        if (user == null)
        {
            // Xử lý người dùng không đăng nhập...
            return RedirectToAction("Index");
        }

        order.UserId = user.Id;
        order.OrderDate = DateTime.UtcNow;
        order.TotalPrice = cart.TotalPrice();
        order.OrderDetails = cart.Items.Select(i => new OrderDetail
        {
            ProductId = i.ProductId,
            Quantity = i.Quantity,
            Price = i.Price
        }).ToList();

        _context.Orders.Add(order);
        await _context.SaveChangesAsync();

        HttpContext.Session.Remove("Cart");

        return RedirectToAction("OrderCompleted", new { orderId = order.Id });
    }

    public IActionResult AddToCart(int productId, int quantity)
    {
        var product = GetProductFromDatabase(productId);
        var cartItem = new CartItem
        {
            ProductId = productId,
            Name = product.Name,
            Price = product.Price,
            Quantity = quantity,
            ImageUrl = product.ImageUrl
        };
        var cart = HttpContext.Session.GetObjectFromJson<ShoppingCart>("Cart") ?? new ShoppingCart();
        cart.AddItem(cartItem);
        HttpContext.Session.SetObjectAsJson("Cart", cart);
        return RedirectToAction("Index");
    }

    public IActionResult Index()
    {
        var cart = HttpContext.Session.GetObjectFromJson<ShoppingCart>("Cart") ?? new ShoppingCart();
        return View(cart);
    }

    public IActionResult RemoveFromCart(int productId)
    {
        var cart = HttpContext.Session.GetObjectFromJson<ShoppingCart>("Cart") ?? new ShoppingCart();
        cart.RemoveItem(productId);
        HttpContext.Session.SetObjectAsJson("Cart", cart);
        return RedirectToAction("Index");
    }

    public IActionResult UpdateCartItemQuantity(int productId, int newQuantity)
    {
        var cart = HttpContext.Session.GetObjectFromJson<ShoppingCart>("Cart") ?? new ShoppingCart();
        cart.UpdateQuantity(productId, newQuantity);
        HttpContext.Session.SetObjectAsJson("Cart", cart);
        return RedirectToAction("Index");
    }

    public IActionResult ClearCart()
    {
        HttpContext.Session.Remove("Cart");
        return RedirectToAction("Index");
    }

    private Product GetProductFromDatabase(int productId)
    {
        return _context.Products.FirstOrDefault(p => p.Id == productId);
    }

    public IActionResult OrderCompleted(int orderId)
    {
        var order = _context.Orders
            .Include(o => o.OrderDetails)
            .ThenInclude(od => od.Product)
            .FirstOrDefault(o => o.Id == orderId);

        if (order == null)
        {
            // Xử lý nếu không tìm thấy đơn hàng
            return RedirectToAction("Index");
        }

        return View(order);
    }
	[Authorize(Roles = SD.Role_Admin)]
	public IActionResult OrderList()
	{
		var orders = _context.Orders.ToList();
		return View(orders);
	}

	public IActionResult OrderDetails(int orderId)
	{
		var orderDetails = _context.OrderDetails
			.Include(od => od.Product)
			.Where(od => od.OrderId == orderId)
			.ToList();

		if (orderDetails == null || !orderDetails.Any())
		{
			// Xử lý trường hợp không có chi tiết đơn hàng nào được tìm thấy
			return RedirectToAction("Index", "ShoppingCart");
		}

		return View(orderDetails);
	}
}