﻿using WebBanHang.Models;
using System.Linq;
namespace WebBanHang.Repositories
{
    public interface IProductRepository
    {
        Task<IEnumerable<Product>> GetAllAsync();
        Task<Product> GetByIdAsync(int id);
        Task AddAsync(Product product);
        Task UpdateAsync(Product product);
        Task DeleteAsync(int id);

		Task<IEnumerable<Product>> SearchAsync(string search);
	}

}
